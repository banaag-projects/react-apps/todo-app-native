import { configureStore } from "@reduxjs/toolkit";
import todoSlice from "./todoSlice";

const store = configureStore({
  reducer: {
    todosReducer: todoSlice.reducer,
  },
});

export default store;
