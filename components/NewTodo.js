import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { todoActions } from "../todoSlice";
import {
  View,
  TextInput,
  TouchableOpacity,
  Text,
  StyleSheet,
} from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#fff",
    borderWidth: 1,
    borderColor: "#ccc",
    paddingVertical: 5,
  },
  input: {
    width: "90%",
    height: 50,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#ccc",
    paddingHorizontal: 20,
    marginBottom: 20,
  },
  button: {
    width: "90%",
    height: 50,
    backgroundColor: "#2196F3",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  buttonText: {
    color: "#fff",
    fontSize: 18,
  },
});

function NewTodo() {
  //VARIABLE DECLARATIONS
  const dispatch = useDispatch();
  const [todoName, setTodoName] = useState("");

  //FUNCTIONS

  const addTodo = () => {
    dispatch(todoActions.createTodo({ name: todoName, id: Math.random() }));
    setTodoName("");
  };

  const todos = useSelector((state) => state.todosReducer.todosArray);

  console.log(todos);

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.input}
        placeholder="Add new todo"
        value={todoName}
        onChangeText={setTodoName}
      />
      <TouchableOpacity style={styles.button} onPress={addTodo}>
        <Text style={styles.buttonText}>ADD TODO</Text>
      </TouchableOpacity>
    </View>
  );
}

export default NewTodo;
