import { todoActions } from "../todoSlice";
import { useSelector, useDispatch } from "react-redux";
import {
  View,
  TextInput,
  Text,
  StyleSheet,
  FlatList,
  TouchableOpacity,
} from "react-native";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import { useState } from "react";

// STYLESHEET
const styles = StyleSheet.create({
  container: {
    flex: 3,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
  },
  item: {
    flex: 1,
    padding: 10,
    marginVertical: 5,
    width: 370,
    borderWidth: 1,
    borderColor: "#ccc",
  },
  row: {
    flex: 1,
    alignItems: "center",
    flexDirection: "row",
    margin: 10,
  },
  buttons: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  input: {
    width: "70%",
    height: 50,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "#ccc",
    paddingHorizontal: 20,
    marginLeft: 6,
  },
  comment: {
    flex: 1,
    padding: 10,
    marginVertical: 5,
    width: 370,
  },
});

function TodoList() {
  //VARIABLE DECLARATIONS
  const todos = useSelector((state) => state.todosReducer.todosArray);
  const dispatch = useDispatch();
  const [updatedTodo, setUpdatedTodo] = useState("");
  const [comment, setComment] = useState("");

  //FUNCTIONS
  function completed(itemId, value) {

    dispatch(
      todoActions.markAsComplete({ id: itemId, isCompleteValue: value })
    );
  }

  function removeTodo(id) {
    dispatch(todoActions.removeTodo(id));
  }

  function handleEditButton(itemId, value) {

    dispatch(
      todoActions.handleToBeUpdated({ id: itemId, showUpdateValue: value })
    );
  }

  function handleEditDoneButton(itemId) {
    dispatch(
      todoActions.handleToBeUpdatedDone({ id: itemId, editedName: updatedTodo })
    );
    setUpdatedTodo("");
  }

  function editComment(e, itemId) {
    setComment(e);
    dispatch(todoActions.editComment({ editedComment: comment, id: itemId }));
  }

  function showComment(itemId, value) {

    dispatch(
      todoActions.showComment({
        showCommentState: value,
        id: itemId,
      })
    );
  }

  // -----------------------------------------
  return (
    <View style={styles.container}>
      <FlatList
        data={todos}
        renderItem={({ item }) => (
          //LIST OF TODOS
          <View style={styles.item}>
            {/* touchable opacity so if the todo is pressed, comments will be shown */}
            <TouchableOpacity
              onPress={() => showComment(item.id, item.isCommentShown)}
            >
              <View style={styles.row}>
                {/* Blue check if task is done, gray check if not done */}
                {item.isComplete === true ? (
                  <FontAwesome
                    name="check-circle"
                    size={25}
                    style={{ marginRight: 15 }}
                    color="#2196F3"
                    onPress={() => completed(item.id, item.isComplete)}
                  />
                ) : (
                  <FontAwesome
                    name="check-circle"
                    size={25}
                    style={{ marginRight: 15 }}
                    color="gray"
                    onPress={() => completed(item.id, item.isComplete)}
                  />
                )}

                {item.toBeUpdated ? (
                  <TextInput
                    style={styles.input}
                    placeholder={item.todoName}
                    value={updatedTodo}
                    onChangeText={setUpdatedTodo}
                  />
                ) : (
                  <Text>{item.todoName}</Text>
                )}

                {/* BUTTONS FOR EDIT AND DELETE TODOS */}

                <View style={styles.buttons}>
                  {item.toBeUpdated ? (
                    <FontAwesome
                      name="check"
                      size={20}
                      color="#000"
                      style={{ marginRight: 15 }}
                      onPress={() => handleEditDoneButton(item.id)}
                    />
                  ) : (
                    <FontAwesome
                      name="edit"
                      size={20}
                      style={{ marginRight: 15 }}
                      onPress={() => {
                        handleEditButton(item.id, item.toBeUpdated);
                      }}
                    />
                  )}

                  <FontAwesome
                    name="trash"
                    size={20}
                    onPress={() => {
                      removeTodo(item.id);
                    }}
                  />
                </View>
              </View>
              {/* comment appear/disappear */}
              {item.isCommentShown && (
                <View style={styles.comment}>
                  <TextInput
                    style={styles.input}
                    placeholder="Add comment..."
                    value={comment}
                    onChangeText={(e) => editComment(e, item.id)}
                  />
                </View>
              )}
            </TouchableOpacity>
          </View>
        )}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
}

export default TodoList;
