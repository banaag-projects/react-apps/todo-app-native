import { useState } from "react";
import NewTodo from "./NewTodo";
import {
  View,
  TextInput,
  TouchableOpacity,
  Text,
  StyleSheet,
} from "react-native";
import TodoList from "./TodoList";

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

function TodoPage() {
  return (
    <View style={styles.container}>
      <NewTodo />
      <TodoList />
    </View>
  );
}

export default TodoPage;
