import { Provider } from "react-redux";
import store from "./store";

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { View } from "react-native";

import Login from "./components/Login";
import NewTodo from "./components/NewTodo";
import TodoList from "./components/TodoList";
import TodoPage from "./components/TodoPage";

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen
          name="TodoPage"
          component={TodoPage}
          options={{
            title: "ToDo Page",
            headerLeft: null, // hide the back button
          }}
        />

        <Stack.Screen name="TodoList" component={TodoList} />
        <Stack.Screen name="NewTodo" component={NewTodo} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default () => {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
};
